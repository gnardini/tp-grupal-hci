function getGET(){
	var lock=document.location.href;
	var getString=lock.split('?');
	if(getString.length == 1) return {};
	var GET=getString[1].split('&');
	var get={};

	for (var i = 0 ; i<GET.length ; i++) {
		var tmp=GET[i].split('=');
		get[tmp[0]]=unescape(decodeURI(tmp[1]));
	}
	return get;
}

function confirmOrder(){
	var get=getGET();

	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=ConfirmOrder&username="+ localStorage.username +"&authentication_token=" + localStorage.auttoken +
		"&order={\"id\":"+ localStorage.cartID +",\"address\":{\"id\":"+ get.address +"}" + (get.card!=undefined? (",\"creditCard\":{\"id\":"+get.card+"}}") : "}"),
		dataType:"jsonp"
	}).done(function(data){
		if(data.error == undefined){
			createCart();
			sendNotification("Compra realizada con exito",'success', 2000); // TODO: Agregar mensaje apropiado
			window.location = 'index.html';
		}
		else sendNotification("Operacion inválida", 'danger', 2000);
	});
}

function createCart(){
    $.ajax({
        url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
        dataType:"jsonp"
    }).done(function(data){
        localStorage.cartID = data.order.id;
    });
}

function showElements(){
$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + localStorage.cartID,
		dataType:"jsonp"
	}).done(function(data){
		var quantity=0;
		var subtotal=0;
		for(var i = 0 ; i < data.order.items.length ; i++){
			$("#lista-pedidos").append('<div class="pedidos-entry"><div class="row">'+
				'<div class="col-md-2"><span>'+ data.order.items[i].product.id +'</span></div>' +
				'<div class="col-md-5"><span>'+ data.order.items[i].product.name +'</span></div>' +
		        '<div class="col-md-2"><span>'+ data.order.items[i].quantity +'</span></div>' +
		        '<div class="col-md-3"><span>$'+ data.order.items[i].price.toFixed(2) +'</span></div>');
			quantity += data.order.items[i].quantity;
			subtotal += data.order.items[i].price * data.order.items[i].quantity;
		}
		
		if(subtotal>0){
			showTax(subtotal*0.21);
			var total = 1.21*subtotal;
			total += 50;
		}
		$("#quantity").append('<span>' + quantity + '</span>');
		$("#total").append('<span>$' + total.toFixed(2) + '</span>');
	});
}

function showTax(impuestos){
	$("#lista-pedidos").append('<div class="pedidos-entry"><div class="row">'+
				'<div class="col-md-2"><span>'+ '-' +'</span></div>' +
				'<div class="col-md-5"><span>'+ 'Impuestos' +'</span></div>' +
		        '<div class="col-md-2"><span>'+ '1' +'</span></div>' +
		        '<div class="col-md-3"><span>$'+ impuestos.toFixed(2) +'</span></div>');
	$("#lista-pedidos").append('<div class="pedidos-entry"><div class="row">'+
				'<div class="col-md-2"><span>'+ '-' +'</span></div>' +
				'<div class="col-md-5"><span>'+ 'Env&iacute;o' +'</span></div>' +
		        '<div class="col-md-2"><span>'+ '1' +'</span></div>' +
		        '<div class="col-md-3"><span>$'+ (50).toFixed(2) +'</span></div>');
}

$(document).ready(function(){
	showElements();

});
