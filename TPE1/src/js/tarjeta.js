var inp = [false, false, false];

function getGET(){
	var lock=document.location.href;
	var getString=lock.split('?')[1];
	if(!getString) return "";
	var GET=getString.split('&');
	var get={};

	for (var i = 0, l=GET.length;i<l; i++) {
		var tmp=GET[i].split('=');
		get[tmp[0]]=unescape(decodeURI(tmp[1]));
	}
	return get;
}

function valnumero(ignorempty){
	var ntar = JSON.parse(localStorage.ntar);
	var elem = document.getElementById("numero");
	ntar.number = elem.value;
	localStorage.setItem('ntar', JSON.stringify(ntar));
	var errmes = 'Se acceptan solo caracteres numéricos de con una longitud dependiente de la marca';
	var valid = false;
	if(!(ignorempty && !elem.value)){
		if( /^\d+$/.test(elem.value)){
			if(elem.value.indexOf('34') == 0 || elem.value.indexOf('37') == 0 ){
				if(elem.value.length == 15){
					valid = true;
				}else{
					errmes = 'La longitud del número no es valido para una American Express';
				}
			}else if(elem.value.indexOf('36') == 0){
				if(elem.value.length == 16){
					valid = true;
				}else{
					errmes = 'La longitud del número no es valido para una Diners';
				}
			}else if(elem.value.indexOf('51') == 0 || elem.value.indexOf('52') == 0 || elem.value.indexOf('53') == 0){
				if(elem.value.length == 16){
					valid = true;
				}else{
					errmes = 'La longitud del número no es valido para una Mastercard';
				}
			}else if(elem.value.indexOf('4') == 0){
				if(elem.value.length == 16 || elem.value.length == 13){
					valid = true;
				}else{
					errmes = 'La longitud del número no es valido para una Visa';
				}
			}
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML).indexOf('Se acceptan solo caracteres num') == 0 || 
					toHTML(mes.innerHTML).indexOf('La longitud del n') == 0)
				mes.style['display']='none';
			valcodseg();
			elem.style["border-color"]= '#5cb85c';
			inp[0] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[0] = false;
		}
	}
}

function valcodseg(ignorempty){
	var ntar = JSON.parse(localStorage.ntar);
	var elem = document.getElementById("codseg");
	var elemca = document.getElementById("numero");
	ntar.securityCode = elem.value;
	localStorage.setItem('ntar', JSON.stringify(ntar));
	var errmes = 'Se acceptan solo caracteres numéricos de con una longitud dependiente de la marca';
	var valid = false;
	if(!(ignorempty && !elem.value)){
		if( /^\d+$/.test(elem.value)){
			if(elemca.value.indexOf('34') == 0 || elemca.value.indexOf('37') == 0 ){
				if(elem.value.length == 4){
					valid = true;
				}else{
					errmes = 'La longitud del código de seguridad no es valido para una American Express';
				}
			}else{
				if(elem.value.length == 3){
					valid = true;
				}else{
					errmes = 'La longitud del código de seguridad no es valido';
				}
			}
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML).indexOf('Se acceptan solo caracteres num') == 0 || 
					toHTML(mes.innerHTML).indexOf('La longitud del c') == 0)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[1] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[1] = false;
		}
	}
}

function valvenci(ignorempty){
	var ntar = JSON.parse(localStorage.ntar);
	var elem = document.getElementById("venci");
	ntar.expirationDate = elem.value;
	localStorage.setItem('ntar', JSON.stringify(ntar));
	var errmes = 'Se acceptan solo vencimientos en la forma indicada';
	var valid = false;
	if(!(ignorempty && !elem.value)){
		if(elem.value.length == 5 && elem.value.indexOf("/") == 2){
			var now = new Date();
			var dos = elem.value.split("/");
			if(dos[0].indexOf("0") == 0)
				dos[0] = dos[0].charAt(1);
			if(dos[0] > 0 && dos[0] < 13)
				if("20"+dos[1] > now.getFullYear() || ("20"+dos[1] == now.getFullYear()  && dos[0] >= now.getMonth()+1)){
					valid=true;
				}else{
					var errmes = 'La fecha de vencimiento tiene que ser futura';
				}
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML).indexOf('Se acceptan solo vencimientos en la forma indicada') == 0 
					|| toHTML(mes.innerHTML).indexOf('La fecha de vencimiento tiene que ser futura') == 0)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[2] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[2] = false;
		}
	}
}
function newTarjeta(elem){
	var url = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateCreditCard&username='+localStorage.username
	+'&authentication_token='+localStorage.auttoken+'&credit_card={"number":"'+elem.number+'","expirationDate":"'+elem.expirationDate 
	+'","securityCode":"'+elem.securityCode+'"}';
	$.ajax({
 		url:url,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(data.creditCard){
 			
 			var iu = JSON.parse(localStorage.infousuar);
 			iu.tar[data.creditCard.id] = data.creditCard;
 			iu.tar[data.creditCard.id].state = 'act';
 			localStorage.setItem('infousuar', JSON.stringify(iu));
 			delete localStorage['ntar'];
 			back();
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado agregando la tarjeta, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado agregando la tarjeta, intente m&aacute;s tarde";
 			}
 			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}

function updTarjeta(elem){
	$.ajax({
		
		url:'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateCreditCard&username='+localStorage.username
			+'&authentication_token='+localStorage.auttoken+'&credit_card={"id":'+elem.id+',"number":"'+elem.number
			+'","expirationDate":"'+elem.expirationDate+'","securityCode":"'+elem.securityCode+'"}',
 		dataType:"jsonp"
 	}).done(function(data){
 		if(!data.error){
 			var iu = JSON.parse(localStorage.infousuar);
 			iu.tar[elem.id] = elem;
 			iu.tar[elem.id].state = 'act';
 			localStorage.setItem('infousuar', JSON.stringify(iu));
 			delete localStorage['ntar'];
 			back();
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cambiando la tarjeta, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cambiando la tarjeta, intente m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}
function getTarjetas(nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.creditCards){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.tar = new Object();
 	 	 			$.each(data.creditCards, function (index, credits){
 	 	 				credits.state = 'act';
 	 	 				infousuar.tar[credits.id] =credits;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 	 			
 	 			}else{
 	 				getTarjetas(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus tarjetas, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}
function getAdresses( nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.addresses){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.dir = new Object();
 	 	 			$.each(data.addresses, function (index, adress){
 	 	 				adress.state = 'act';
 	 	 				infousuar.dir[adress.id] =adress;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 			}else{
 	 				getAdresses(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus direcciones, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}
$(document).ready(function(){
	$('#venci').blur(function(){valvenci();});
	$('#codseg').blur(function(){valcodseg();});
	$('#numero').blur(function(){valnumero();});
	$('#canc').click(function(){localStorage.removeItem('ntar');back();});
	if(!localStorage.infousuar){
		$.ajax({
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAccount&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}).done(function(data){
	 		if(data.account){
		 		var usuario = data.account;
		 		var infousuar = {
		 				"usuario": usuario,
		 				"dir": null,
		 				"tar": null
		 		}
	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
		 		getAdresses();
		 		getTarjetas();
	 			
	 		}else{
	 			var mess;
	 			if(data.error.code){
	 				switch(data.error.code){
	 				case 999:
	 					mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
	 					break;
	 				case 101:
	 				case 102:
	 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
			 			setTimeout(function (){
			 				loggout();
			 			},2000);
	 				break;
	 				default:

						mess = data.error.message;
	 				}
	 			}else{
	 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
	 			}
				sendNotification(mess, 'danger', 3000);
	 			
	 		}
	 	});}
	$('#conf').click(function(){
		valvenci();
		valcodseg();
		valnumero();
		if(inp[0] && inp[1] && inp[2]){
			var infousuar = JSON.parse(localStorage.infousuar);
			if(getGET().id){
				var tar = JSON.parse(localStorage.ntar);
				tar.expirationDate = tar.expirationDate.substring(0,2) + tar.expirationDate.substring(3,5); 
				updTarjeta(tar);
				
			}else{
				var tar = JSON.parse(localStorage.ntar);
				tar.expirationDate = tar.expirationDate.substring(0,2) + tar.expirationDate.substring(3,5);
				tar.state = "new";
				newTarjeta(tar);
			}
		}else{
			sendNotification("Tienes que llenar todo el formulario correctamente antes de guardar", 'danger', 3000);
		}
	});
	var ntar = null;
	if(typeof localStorage.ntar !== "undefined"){
		ntar = JSON.parse(localStorage.ntar);
	}
	if(getGET().id){
		if(!ntar || ntar.id != getGET().id){
			ntar = JSON.parse(localStorage.infousuar).tar[getGET().id];
			var exp = ntar.expirationDate;
			ntar.expirationDate =[exp.slice(0, 2), '/', exp.slice(2)].join('');
		}
		localStorage.setItem('ntar', JSON.stringify(ntar));
		$("#titulo").text('Cambiar tarjeta de crédito');
	}else if(!ntar){
		ntar = {"expirationDate":"", "securityCode":"","number":"","state":"new"};
		localStorage.setItem('ntar', JSON.stringify(ntar));
	}

	$('#venci').val( ntar.expirationDate);
	$('#codseg').val(ntar.securityCode);
	$('#numero').val(ntar.number);
	valvenci(true);
	valcodseg(true);
	valnumero(true);
	
});