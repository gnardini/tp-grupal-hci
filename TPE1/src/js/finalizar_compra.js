$(document).ready(function(){

	$.ajax({
        url: "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username+"&authentication_token=" + localStorage.auttoken,
        dataType:"jsonp"
    }).done(function(data){
        for(var i = 0 ; i < data.addresses.length ; i++){
        	$("#direcciones").append('<option class="address" id="'+data.addresses[i].id+'">'+ data.addresses[i].name + ': ' + data.addresses[i].street + ' ' + data.addresses[i].number +'</option>');
        }
        document.getElementById("direcciones").selectedIndex = -1;
    });

    $.ajax({
        url: "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username+"&authentication_token=" + localStorage.auttoken,
        dataType:"jsonp"
    }).done(function(data){
        for(var i = 0 ; i < data.creditCards.length ; i++){
        	$("#tarjetas").append('<option class="card" id="'+data.creditCards[i].id+'">'+ data.creditCards[i].number + '-XXX</option>');
        }
        document.getElementById("tarjetas").selectedIndex = -1;
    });

});

function finalizar(){
	var dir = document.getElementsByClassName('input-direccion');
	var tar = document.getElementsByClassName('input-tarjeta');
	var cup = document.getElementsByClassName('input-cupon');
	if((tar[0].checked || tar[1].checked) && (cup[0].checked || cup[1].checked)){
		//TODO: Elegir bien las opciones.
		var address;
		var card;
		var adds = document.getElementsByClassName('address');
		for(var i=0 ; i<adds.length ; i++){
			if(adds[i].selected){
				address = adds[i].id;
			}
		}
		if(address == undefined){
			sendNotification("Debes seleccionar una dirección de envío", 'danger', 4000);
			return;
		}
		if(tar[0].checked){
			var cards = document.getElementsByClassName('card');
			for(var i=0 ; i<cards.length ; i++){
				if(cards[i].selected){
					card = cards[i].id;
				}
			}
			if(card == undefined){
				sendNotification("Debes seleccionar una forma de pago válida", 'danger', 4000);
				return;
			}
		}
		
		window.location = 'resumen.html?&address=' + address + (card != undefined ? ('&card=' + card) : '');
	}else{
		sendNotification("Debes seleccionar una opción en cada uno de los pasos", 'danger', 3500);
	}
}