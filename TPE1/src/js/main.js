//Para saber si esta logueado o no.
var logged=false;

function isLogged(){
    return logged;
}

/**
 * Archivo central para algunas funciones útiles 
 **/

/*
 * Para ir a un otro Documento o pagina web
 */
 function link(page){
 	window.location=page;
 }

/*
 * Para retrasar en la historia del browser, úsar cuando se quiera volver
 */
 function back(){
 	history.go(-1);
 	return true;
 }


 /**
  * Todos los metodos de autenticación y camio de estado en el header.
  * @returns {Boolean}
  */

  function setHeaderlogin(){
      document.getElementById("login-dd").style["display"] = 'none';
      document.getElementById("logged-dd").style["display"] = 'inline';	
   // document.getElementById("mostrar-carrito").style["display"] = 'inline'; TODO: SACAR COMENTARIO
}

function setHeaderloggout(){
  document.getElementById("login-dd").style["display"] = 'inline';
  document.getElementById("logged-dd").style["display"] = 'none';
   // document.getElementById("mostrar-carrito").style["display"] = 'none'; TODO: SACAR COMENTARIO
}

function login(){
  var user = $('#usuario').val();
  var pass = $('#contra').val();
  if(user && pass){
   $.ajax({
    url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=SignIn&username="+user+"&password="+pass,
    dataType:"jsonp"
}).done(function(data){
    if(data.authenticationToken){
     localStorage.username = data.account.username;
     localStorage.auttoken = data.authenticationToken;
     setCartID();
     setHeaderlogin();
     sendNotification("Ha iniciado sesión con éxito", 'info', 2000);
     logged=true;        
 }else{
     switch(data.error.code){
         case 2:
         case 3:
         case 101:
         case 104:
         case 105:
         sendNotification("Combinación de usuario y contrase&ntilde;a inv&aacute;lida", 'danger', 3000);
         break;
         case 104:
         sendNotification("Error desconocido, por favor cont&aacute;ctenos", 'danger', 3000);
         break;
         default:
         sendNotification("Error desconocido, int&eacute;ntalo m&aacute;s tarde", 'danger', 3000);
     }
 }
});
}else{
   sendNotification("El nombre de usuario y la contrase&ntilde;a son necesarios", 'danger', 3000);
}
}

function loggout(){
  $.ajax({
   url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=SignOut&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
   dataType:"jsonp"
}).done(function(data){
   if(data.error != undefined){
    if(data.error.code == 999){
        sendNotification("Error desconocido mientas se cerraba la sesi&oacute;n, intente m&aacute;s tarde", 'danger', 3000);
    }
}else{
    localStorage.removeItem('auttoken');
    localStorage.removeItem('username');
    localStorage.removeItem('cartID');
    setHeaderlogin();
    window.location = 'index.html';
}
});
return false;
}

function setCartID(){
   // alert("http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken);
    $.ajax({
        url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
        dataType:"jsonp"
    }).done(function(data){
        if(data.orders.length == 0){
            createCart();
        }//sendNotification("Error al crear la cuenta, lo sentimos!", 'danger', 3000);
        else{
            for(var i=0 ; i<data.orders.length;i++){
                if(data.orders[i].status==1){
                    localStorage.cartID = data.orders[i].id;
                    return;
                }
            }
            createCart();
        }
    });
}
function fill(a){
	if(a.length ==1)
		return "0"+a;
	return a;
}

function datetoclient(date){
  if(!date)
   return ""
var from = date.split("-");
return from[2] +"/"+fill(from[1] )+"/"+ fill(from[0]);;
}
function datetoserver(date){
    if(!date)
     return ""
 var from = date.split("/");
 return from[2] +"-"+fill(from[1] )+"-"+ fill(from[0]);
}

 /**
  * Función para enviar una Notification abajo del header.
  * @param message - Mensaje de mostrar
  * @param status - estado de la notificación
  * - success, danger, warning y info
  * @param vanishtime - tiempo después de que desaparece en milisegundos 3000 son 3 segundos
  */
  function sendNotification(message, status, vanishtime){
      document.getElementById("messages").innerHTML = message;
      document.getElementById("messages").style["display"] = 'inline';
      $('#messages').removeClass("alert-info").removeClass("alert-warning").removeClass("alert-danger").removeClass("alert-success").addClass("alert-"+status);
      if(vanishtime)
       setTimeout(function(){
        document.getElementById("messages").style["display"] = 'none';
    }, vanishtime);
}

$(document).ready(function() { 
    // alert(localStorage.username);
    // alert(localStorage.auttoken);
    if(localStorage.auttoken != undefined){
       // alert("Entra");
       setHeaderlogin();
   }else{
    setHeaderloggout();
}
setCartID();
showCartElements();
var path = $(location).attr('pathname');
if(path.indexOf('editar_usuario.html') < 0 && path.indexOf('cambiar_contra.html')  < 0  
   && path.indexOf('tarjeta.html')  < 0  && path.indexOf('direccion.html')  < 0 ) 
   localStorage.removeItem('infousuar');
if(path.indexOf('tarjeta.html')  < 0) 
   localStorage.removeItem('ntar');
if(path.indexOf('direccion.html')  < 0){
  localStorage.removeItem('ndir');
}
if(path.indexOf('cambiar_contra.html')  < 0){
  localStorage.removeItem('ncon');
}
$('#login-dropdown').click(function(){localStorage.backto=document.location.href;link('iniciar_sesion.html?iniciar=true');return false;});
$('#entrar-login').click(function(){login();return false;});
$('#cerrar-login').click(function(){loggout();return false;});
setInterval(function(){
 if(!navigator.onLine){
    sendNotification("La conexi&oacute;n no parece funcionar, verif&iacute;calo o intente m&aacute;s tarde", 'danger')
}else{
    if(document.getElementById("messages").innerHTML == "La conexi&oacute;n no parece funcionar, verif&iacute;calo o intente m&aacute;s tarde")
    document.getElementById("messages").style["display"] = 'none';
}
},1000);

});


// function enableCart(){
//     if ($('.carrito_todo').is(':empty')){
//        return;
//    }
//    document.getElementsByClassName(".carrito_todo").className = "dropdown dropdown-medium dropdown-icon carrito_todo";
//    $(".carrito_todo").append('<a href="#"  class="dropdown-toggle" data-toggle="dropdown" onclick="link(\'carrito.html\');">'+
//       '<img src="./img/carrito2.png"  alt="Carrito" class="deseos_y_carrito_foto">'+
//       '<span class="badge" id="carrito-cantidad"></span>'+
//       '</a>'+
//       '<ul class="dropdown-menu dropdown-menu-medium" role="menu">'+
//       '<li class="justified" id="carrito-header"><b>En tu carrito :</b></li>'+
//       '<li><div>'+
//       '<button  type="button" class="btn btn-info buttonDropd" onclick="link(\'carrito.html\');">Abrir el carrito de compras</button>'+
//       '</div></li></ul>');
// }

function showCartElements(){
    //alert("http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + localStorage.cartID);
    //alert(localStorage.cartID);
    if(localStorage.username != undefined){
        $.ajax({
            url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + localStorage.cartID,
            dataType:"jsonp"
        }).done(function(data){
            $("#carrito-cantidad").text(data.order.items.length);
            for(var i = 0 ; i < data.order.items.length ; i++){
             $("#carrito-header").after('<li class="row producto"><div class="col-xs-4 col-md-4"><a href="producto.html?id='+ data.order.items[i].product.id+'">' +
                '<img alt="'+data.order.items[i].product.name+'" src="'+data.order.items[i].product.imageUrl+'" class="carrito_img_header"></a></div>' + 
                '<div class="col-xs-8 col-md-8"> <a href="producto.html?id='+ data.order.items[i].product.id+'">'+data.order.items[i].product.name+'</a></div></li>');
         }
     });
    }
    
}


/*
    <li class="row producto">
        <div class="col-xs-4 col-md-4">
            <a href="producto.html">
                <img alt="Camisa elegante" src="img/camisalacoste.jpg" class="carrito_img_header">
            </a>
        </div>
        <div class="col-xs-8 col-md-8">
            <a href="producto.html">Camisa Lacoste</a>
            <p>Cantidad<span class="badge badgeprod">1</span></p>
        </div>
    </li>
    */

/*
    $("#carrito").append('<tr><td><div><div class="col-md-6"><img alt="' + data.order.items[i].product.name + '" src="' + data.order.items[i].product.imageUrl + '" class="producto-img"></div>' + 
        '<div class="col-md-6"><p class="producto-titulo">' + data.order.items[i].product.name + '</p>' + //TODO: Agregar la descripcion/info del producto
        '<button type="button" class="btn btn-warning">Agregar a deseos</button><button type="button" class="btn btn-danger" onclick="deleteItem('+ data.order.items[i].id +')">Eliminar</button></div></div></td>' + 
        '<td><dt class="celda-precio"><ul><div class="col-md-6"><input type="text" value="'+ data.order.items[i].quantity +'" class="input-cant"></div><div class="col-md-6">' + 
        '<button type="button" class="btn btn-default btn-md"><span class="glyphicon glyphicon-chevron-up"></button><button type="button" class="btn btn-default btn-md"><span class="glyphicon glyphicon-chevron-down"></button></div></ul></dt></td>' + 
        '<td><p class="celda-precio">$'+ data.order.items[i].price +'</p></td><td><p class="celda-precio">$'+ data.order.items[i].quantity*data.order.items[i].price +'</p></td></tr>'
        );
*/


function toHTML(texto){
   return texto.replace(/[\u00A0-\u2666<>\&]/g, function(c) {
     return '&' + 
     (escapeHtmlEntities.entityTable[c.charCodeAt(0)] || '#'+c.charCodeAt(0)) + ';';
 });
}

function createCart(){
    $.ajax({
        url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
        dataType:"jsonp"
    }).done(function(data){
        localStorage.cartID = data.order.id;
    });
}

 //all HTML4 entities as defined here: http://www.w3.org/TR/html4/sgml/entities.html
 //added: amp, lt, gt, quot and apos
 var escapeHtmlEntities = { entityTable: {
    34 : 'quot', 
    38 : 'amp', 
    39 : 'apos', 
    60 : 'lt', 
    62 : 'gt', 
    160 : 'nbsp', 
    161 : 'iexcl', 
    162 : 'cent', 
    163 : 'pound', 
    164 : 'curren', 
    165 : 'yen', 
    166 : 'brvbar', 
    167 : 'sect', 
    168 : 'uml', 
    169 : 'copy', 
    170 : 'ordf', 
    171 : 'laquo', 
    172 : 'not', 
    173 : 'shy', 
    174 : 'reg', 
    175 : 'macr', 
    176 : 'deg', 
    177 : 'plusmn', 
    178 : 'sup2', 
    179 : 'sup3', 
    180 : 'acute', 
    181 : 'micro', 
    182 : 'para', 
    183 : 'middot', 
    184 : 'cedil', 
    185 : 'sup1', 
    186 : 'ordm', 
    187 : 'raquo', 
    188 : 'frac14', 
    189 : 'frac12', 
    190 : 'frac34', 
    191 : 'iquest', 
    192 : 'Agrave', 
    193 : 'Aacute', 
    194 : 'Acirc', 
    195 : 'Atilde', 
    196 : 'Auml', 
    197 : 'Aring', 
    198 : 'AElig', 
    199 : 'Ccedil', 
    200 : 'Egrave', 
    201 : 'Eacute', 
    202 : 'Ecirc', 
    203 : 'Euml', 
    204 : 'Igrave', 
    205 : 'Iacute', 
    206 : 'Icirc', 
    207 : 'Iuml', 
    208 : 'ETH', 
    209 : 'Ntilde', 
    210 : 'Ograve', 
    211 : 'Oacute', 
    212 : 'Ocirc', 
    213 : 'Otilde', 
    214 : 'Ouml', 
    215 : 'times', 
    216 : 'Oslash', 
    217 : 'Ugrave', 
    218 : 'Uacute', 
    219 : 'Ucirc', 
    220 : 'Uuml', 
    221 : 'Yacute', 
    222 : 'THORN', 
    223 : 'szlig', 
    224 : 'agrave', 
    225 : 'aacute', 
    226 : 'acirc', 
    227 : 'atilde', 
    228 : 'auml', 
    229 : 'aring', 
    230 : 'aelig', 
    231 : 'ccedil', 
    232 : 'egrave', 
    233 : 'eacute', 
    234 : 'ecirc', 
    235 : 'euml', 
    236 : 'igrave', 
    237 : 'iacute', 
    238 : 'icirc', 
    239 : 'iuml', 
    240 : 'eth', 
    241 : 'ntilde', 
    242 : 'ograve', 
    243 : 'oacute', 
    244 : 'ocirc', 
    245 : 'otilde', 
    246 : 'ouml', 
    247 : 'divide', 
    248 : 'oslash', 
    249 : 'ugrave', 
    250 : 'uacute', 
    251 : 'ucirc', 
    252 : 'uuml', 
    253 : 'yacute', 
    254 : 'thorn', 
    255 : 'yuml', 
    402 : 'fnof', 
    913 : 'Alpha', 
    914 : 'Beta', 
    915 : 'Gamma', 
    916 : 'Delta', 
    917 : 'Epsilon', 
    918 : 'Zeta', 
    919 : 'Eta', 
    920 : 'Theta', 
    921 : 'Iota', 
    922 : 'Kappa', 
    923 : 'Lambda', 
    924 : 'Mu', 
    925 : 'Nu', 
    926 : 'Xi', 
    927 : 'Omicron', 
    928 : 'Pi', 
    929 : 'Rho', 
    931 : 'Sigma', 
    932 : 'Tau', 
    933 : 'Upsilon', 
    934 : 'Phi', 
    935 : 'Chi', 
    936 : 'Psi', 
    937 : 'Omega', 
    945 : 'alpha', 
    946 : 'beta', 
    947 : 'gamma', 
    948 : 'delta', 
    949 : 'epsilon', 
    950 : 'zeta', 
    951 : 'eta', 
    952 : 'theta', 
    953 : 'iota', 
    954 : 'kappa', 
    955 : 'lambda', 
    956 : 'mu', 
    957 : 'nu', 
    958 : 'xi', 
    959 : 'omicron', 
    960 : 'pi', 
    961 : 'rho', 
    962 : 'sigmaf', 
    963 : 'sigma', 
    964 : 'tau', 
    965 : 'upsilon', 
    966 : 'phi', 
    967 : 'chi', 
    968 : 'psi', 
    969 : 'omega', 
    977 : 'thetasym', 
    978 : 'upsih', 
    982 : 'piv', 
    8226 : 'bull', 
    8230 : 'hellip', 
    8242 : 'prime', 
    8243 : 'Prime', 
    8254 : 'oline', 
    8260 : 'frasl', 
    8472 : 'weierp', 
    8465 : 'image', 
    8476 : 'real', 
    8482 : 'trade', 
    8501 : 'alefsym', 
    8592 : 'larr', 
    8593 : 'uarr', 
    8594 : 'rarr', 
    8595 : 'darr', 
    8596 : 'harr', 
    8629 : 'crarr', 
    8656 : 'lArr', 
    8657 : 'uArr', 
    8658 : 'rArr', 
    8659 : 'dArr', 
    8660 : 'hArr', 
    8704 : 'forall', 
    8706 : 'part', 
    8707 : 'exist', 
    8709 : 'empty', 
    8711 : 'nabla', 
    8712 : 'isin', 
    8713 : 'notin', 
    8715 : 'ni', 
    8719 : 'prod', 
    8721 : 'sum', 
    8722 : 'minus', 
    8727 : 'lowast', 
    8730 : 'radic', 
    8733 : 'prop', 
    8734 : 'infin', 
    8736 : 'ang', 
    8743 : 'and', 
    8744 : 'or', 
    8745 : 'cap', 
    8746 : 'cup', 
    8747 : 'int', 
    8756 : 'there4', 
    8764 : 'sim', 
    8773 : 'cong', 
    8776 : 'asymp', 
    8800 : 'ne', 
    8801 : 'equiv', 
    8804 : 'le', 
    8805 : 'ge', 
    8834 : 'sub', 
    8835 : 'sup', 
    8836 : 'nsub', 
    8838 : 'sube', 
    8839 : 'supe', 
    8853 : 'oplus', 
    8855 : 'otimes', 
    8869 : 'perp', 
    8901 : 'sdot', 
    8968 : 'lceil', 
    8969 : 'rceil', 
    8970 : 'lfloor', 
    8971 : 'rfloor', 
    9001 : 'lang', 
    9002 : 'rang', 
    9674 : 'loz', 
    9824 : 'spades', 
    9827 : 'clubs', 
    9829 : 'hearts', 
    9830 : 'diams', 
    338 : 'OElig', 
    339 : 'oelig', 
    352 : 'Scaron', 
    353 : 'scaron', 
    376 : 'Yuml', 
    710 : 'circ', 
    732 : 'tilde', 
    8194 : 'ensp', 
    8195 : 'emsp', 
    8201 : 'thinsp', 
    8204 : 'zwnj', 
    8205 : 'zwj', 
    8206 : 'lrm', 
    8207 : 'rlm', 
    8211 : 'ndash', 
    8212 : 'mdash', 
    8216 : 'lsquo', 
    8217 : 'rsquo', 
    8218 : 'sbquo', 
    8220 : 'ldquo', 
    8221 : 'rdquo', 
    8222 : 'bdquo', 
    8224 : 'dagger', 
    8225 : 'Dagger', 
    8240 : 'permil', 
    8249 : 'lsaquo', 
    8250 : 'rsaquo', 
    8364 : 'euro'
}};