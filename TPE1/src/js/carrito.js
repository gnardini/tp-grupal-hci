$(document).ready(function(){
	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + localStorage.cartID,
		dataType:"jsonp"
	}).done(function(data){
		var subtotal = 0;
		var array=[];
		if(data.order.items.length == 0){
			//TODO: Agregar mensaje apropiado
		}else{
			for(var i = 0 ; i < data.order.items.length ; i++){
				$("#carrito").append('<tr><td><div><div class="col-md-6"><img alt="' + data.order.items[i].product.name + '" src="' + data.order.items[i].product.imageUrl + '" class="producto-img"></div>' + 
					'<div class="col-md-6"><p class="producto-titulo">' + data.order.items[i].product.name+'</p><p class="producto-titulo brand-product-class " id="brand-product'+data.order.items[i].product.id+'"></p>' + //TODO: Agregar la descripcion/info del producto
					'</div></div></td>' + 
					'<td><dt class="celda-precio"><ul><div class="col-md-6" id="loops"><input type="text" value="'+ data.order.items[i].quantity +'" class="input-cant" id="quantity-'+data.order.items[i].id+'"></div><div class="col-md-6">' + 
					'<button type="button" class="btn btn-default btn-md" onclick="increment('+data.order.items[i].id+','+data.order.items[i].product.id+');"><span class="glyphicon glyphicon-chevron-up"></button><button type="button" class="btn btn-default btn-md" onclick="decrement('+data.order.items[i].id+','+data.order.items[i].product.id+');"><span class="glyphicon glyphicon-chevron-down"></button></div></ul></dt></td>' + 
					'<td><p class="celda-precio" id="unique-'+data.order.items[i].id+'">$'+ data.order.items[i].price +'</p></td><td><p class="celda-precio" id="price-'+data.order.items[i].id+'">$'+ (data.order.items[i].quantity*data.order.items[i].price ).toFixed(2)+'</p></td>'+'<td><button type="button" class="btn eliminar" onclick="deleteItem('+ data.order.items[i].id +')"><span class="glyphicon glyphicon-trash"></span></button></tr>');
subtotal += data.order.items[i].price * data.order.items[i].quantity;
getBrand(data.order.items[i].product.id);
}
}

$("#subtotal").append('$' + subtotal.toFixed(2));
$("#impuestos").append('$' + (subtotal*0.21).toFixed(2));
$("#envio").append('$' + (data.order.items.length == 0 ? 0 : 50).toFixed(2));
$("#total-compra").append('$' + (subtotal*1.21 + (data.order.items.length == 0 ? 0 : 50)).toFixed(2));

});
});

function getBrand(data){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+data,
		dataType:"jsonp"
	}).done(function(data2){
		for(var i=0;i<data2.product.attributes.length;i++){			
			if (data2.product.attributes[i].name=="Marca"){
				$("#brand-product"+data).text(data2.product.attributes[i].values[0]);
			}
		}
	});
}

function searchBrand(data){
	for (var i = 0; i <data.filters.length; i++) {
		if (data.filters[i].id=="9")
			return data.filters[i].name;
	}
}

function contains(array,elem){
	for (var i=0; i<array.length;i++){
		if (array[i]==elem)
			return true;
	}
	return false;
}

function deleteItem(id){
	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + id,
		dataType:"jsonp"
	}).done(function(data){
		location.reload();
	});
}

function modifiedItem(id, idProd){
	if(localStorage.username == undefined){
		window.location="iniciar_sesion.html";
	}else{		
		var quantity = $('#quantity-' + id).attr("value");
		$.ajax({
			url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + id,
			dataType:"jsonp"
		}).done(function(data){
			$.ajax({
				url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
				dataType:"jsonp"
			}).done(function(data1){
				$.ajax({
					url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken +
					"&order_item={\"order\":{\"id\":" + localStorage.cartID + "},\"product\":{\"id\":" + idProd + "},\"quantity\":" + quantity + "}",
					dataType:"jsonp"
				}).done(function(data2){
					location.reload();
				});
			});
		});

	}
}

function increment(id,idProd){

	var i=$('#quantity-' + id).attr("value");
	$('#quantity-' + id).attr("value",parseInt(i)+1);
	modifiedItem(id,idProd);
	
	
}

function decrement(id,idProd){
	var i=$('#quantity-' + id).val();
	if (parseInt(i)>1){
		$('#quantity-' + id).attr("value",parseInt(i)-1);
		modifiedItem(id,idProd);
	}
}



/*
function setCartID(){
	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
		dataType:"jsonp"
	}).done(function(data){
		if(data.orders.length == 0) createOrder();
		else localStorage.cartID = data.orders[0].id;
	});
}

function createOrder(){
	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
		dataType:"jsonp"
	}).done(function(data){
		localStorage.cartID = data.order.id;
	});
}
*/