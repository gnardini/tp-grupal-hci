function getData(attr,name,init,end){
	for (var i = 0; i < attr.length; i++) {
		if (attr[i].name.substring(init,end)==name)
			return attr[i];
	};
	return null;
}


$(document).ready(function(){


		cargarNuevos();
		cargarOfertas();

});



function cargarNuevos(){
	$.ajax({
		url:'http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[%20{%20\"id\":%206,%20\"value\":%20\"Nuevo\"%20}%20]&page_size=35',
		dataType:"jsonp"
	}).done(function(data){
		var array=[];
		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#nuevo_carrusel1").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+' class="img_carousel">'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#nuevo_carrusel1").append('<div class="col-md-1"></div>');

		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#nuevo_carrusel2").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+' class="img_carousel">'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#nuevo_carrusel2").append('<div class="col-md-1"></div>');
	});
}

function cargarOfertas(){
	$.ajax({
		url:'http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[%20{%20\"id\":%205,%20\"value\":%20\"Oferta\"%20}%20]&page_size=29',
		dataType:"jsonp"
	}).done(function(data){
		var array=[];
		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#oferta_carrusel1").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+' class="img_carousel">'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#oferta_carrusel1").append('<div class="col-md-1"></div>');

		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#oferta_carrusel2").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+' class="img_carousel">'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#oferta_carrusel2").append('<div class="col-md-1"></div>');
	});
}


function contains(array,elem){
	for(i=0;i<array.length;i++){
		if (array[i]==elem)
			return true;
	}
	return false;
}



