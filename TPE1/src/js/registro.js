var inp = [false, false, false, false, false, false, false, false];
/**
 * 
 */
 function valusuario(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_usuario");
 	nusuario.username = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'El nombre de usuario tiene que ser de entre 6 y 15 caracteres';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length > 5 && elem.value.length < 16){
 			elem.style["border-color"]= '#5cb85c';
 			inp[0] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[0] = false;
 		}
 	}	
 }
 function valcontra(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_contra");
 	nusuario.password = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'La contrase&ntilde;a tiene que ser de entre 8 y 15 caracteres';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length > 7 && elem.value.length < 16){
 			elem.style["border-color"]= '#5cb85c';
 			inp[1] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 			valconfcontra();
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[1] = false;
 		}
 	}
 }
 function valconfcontra(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_confcontra");
 	var errmes = 'La contrase&ntilde;a confirmada no coincide con la contrase&ntilde;a';
 	if(!(ignorempty && !elem.value)){
 		if(nusuario.password == elem.value){
 			elem.style["border-color"]= '#5cb85c';
 			inp[2] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes){
 				mes.style['display']='none';
 			}	
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[2] = false;
 		}
 	}
 }
 function valnombre(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_nombre");
 	nusuario.firstName = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'El nombre no puede exceder los 80 caracteres o estar vac&iacute;o';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length > 0 && elem.value.length <= 80){
 			elem.style["border-color"]= '#5cb85c';
 			inp[3] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[3] = false;
 		}
 	}
 }
 function valapell(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_apell");
 	nusuario.lastName = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'El apellido no puede exceder los 80 caractereso estar vac&iacute;o';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length > 0 && elem.value.length <= 80){
 			elem.style["border-color"]= '#5cb85c';
 			inp[4] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[4] = false;
 		}
 	}	
 }
 function valgenero(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'G&eacute;nero tiene que ser especificado';
 	if($('#genmal').checked){
 		nusuario.gender = "M";
 		var mes = document.getElementById("messages");
 		if(mes.innerHTML == errmes)
 			mes.style['display']='none';
 	}else{
 		if($('#genfem').checked){
 			nusuario.gender = "F";
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			if(!nusuario.gender)
 				sendNotification(errmes, 'danger');
 		}
 	}
 }
 function valdni(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_dni");
 	nusuario.identityCard = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'El DNI pude componerse solo de caracteres num&eacute;ricos con una longitud de hasta 10 posiciones';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length <= 10 && /^\d+$/.test(elem.value)){
 			elem.style["border-color"]= '#5cb85c';
 			inp[5] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[5] = false;
 		}
 	}	
 }
 function valcorreoe(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_correoe");
 	nusuario.email = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes = 'El correo tiene que contener una @ y tener una longitud de hasta 128 posiciones';
 	if(!(ignorempty && !elem.value)){
 		if(elem.value.length <= 128 && /@+/.test(elem.value)){
 			elem.style["border-color"]= '#5cb85c';
 			inp[6] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes)
 				mes.style['display']='none';
 		}else{
 			sendNotification(errmes, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[6] = false;
 		}
 	}
 }
 function valfechanac(ignorempty){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	var elem = document.getElementById("input_fechanac");
 	nusuario.birthDate = elem.value;
 	localStorage.setItem('nusuario', JSON.stringify(nusuario));
 	var errmes1 = 'La fecha de de nacimiento tiene que ser inserida como explicado';
 	var errmes2 = 'Lo sentimos pero los usuarios tienen que tener al menos 16 a&ntilde;os';
 	if(!(ignorempty && !elem.value)){
 		var from = elem.value.split("/");
 		var d = new Date(from[2], from[1] - 1, from[0]);
 		var comp = new Date(d.getFullYear() + 16, from[1] - 1, from[0]);
 		
 		if(elem.value && !isNaN(d.getTime()) && from[2].length >3 && comp <= new Date()){
 			elem.style["border-color"]= '#5cb85c';
 			inp[7] = true;
 			var mes = document.getElementById("messages");
 			if(toHTML(mes.innerHTML) == errmes1 || toHTML(mes.innerHTML) == errmes2)
 				mes.style['display']='none';
 		}else{
 			if(comp > new Date())
 				sendNotification(errmes2, 'danger');
 			else
 				sendNotification(errmes1, 'danger');
 			elem.style["border-color"]= '#d9534f';
 			inp[7] = false;
 		}
 	}
 }




 function register(){
 	var nusuario = JSON.parse(localStorage.nusuario);
 	if(inp[0] && inp[1] && inp[2]&& inp[3]&& inp[4]&& inp[5]&& inp[6]&& inp[7]){
 		$.ajax({
 			url:'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateAccount&account={"username":"'+nusuario.username
 			+'","password":"'+nusuario.password+'","firstName":"'+nusuario.firstName+'","lastName":"'+nusuario.lastName
 			+'","gender":"'+nusuario.gender+'","identityCard":"'+nusuario.identityCard+'","email":"'+nusuario.email
 			+'","birthDate":"'+datetoserver(nusuario.birthDate)+'"}',
 			dataType:"jsonp"
 		}).done(function(data){
 			if(data.account){
 				sendNotification("Registrado con &eacute;xito", 'success', 2000);
 				$.ajax({
 					url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=SignIn&username="
 					+nusuario.username+"&password="+nusuario.password,
 					dataType:"jsonp"
 				}).done(function(data){
 					localStorage.removeItem('nusuario');
 					setTimeout(function(){
 						if(data.authenticationToken){
 							localStorage.username = data.account.username;
 							localStorage.auttoken = data.authenticationToken;
 							createCart();
 							setHeaderlogin();
 							sendNotification("Entrado con &eacute;xito", 'info', 2000);
 							setTimeout(back,1500);
 						}else{
 							switch(data.error.code){
 								case 2:
 								case 3:
 								case 104:
 								case 105:
 								sendNotification("Combinación de usuario y contrase&ntilde;a invalida", 'danger', 3000);
 								break;
 								case 104:
 								sendNotification("Usuario no puede entrar, contactanos para informaciones", 'danger', 3000);
 								break;
 								default:
 								sendNotification("Error desconocido mientas entraste, Logeate m&aacute;s tarde", 'danger', 3000);
 							}
 						}
 					},1500);
 				});
 			}else{
 				var mess;
 				switch(data.error.code){
 					case 999:
 					mess = "Error desconocido, Int&eacute;ntalo m&aacute;s tarde";

 					default:
 					mess = data.error.message;
 				}
 				sendNotification(mess, 'danger', 3000);
 			}
 		});
}else{
	valusuario();
	valcontra();
	valconfcontra();
	valnombre();
	valapell();
	valgenero();
	valdni();
	valcorreoe();
	valfechanac();
	sendNotification("Tienes que llenar todo el formulario correctamente antes de finalizar la registración", 'danger', 3000);
}
return false;
}

function createCart(){
	$.ajax({
		url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
		dataType:"jsonp"
	}).done(function(data){
		localStorage.cartID = data.order.id;
	});
}

$(document).ready(function(){
	if(!localStorage.nusuario){
		nusuario = {
			"username": "",
			"password": "",
			"firstName": "",
			"lastName": "",
			"gender": "M",
			"identityCard": "",
			"email": "",
			"birthDate": ""
		};
		localStorage.setItem('nusuario', JSON.stringify(nusuario));
	}
	var nusuario = JSON.parse(localStorage.nusuario);
	$('#input_usuario').val(nusuario.username);
	$('#input_contra').val(nusuario.password);
	$('#input_nombre').val(nusuario.firstName);
	$('#input_apell').val(nusuario.lastName);
	$('#input_dni').val(nusuario.identityCard);
	$('#input_correoe').val(nusuario.email);
	$('#input_fechanac').val(nusuario.birthDate);
	if(nusuario.gender == 'M'){
		$('#genmal').prop("checked", true);
	}else{
		if(nusuario.gender == 'F')
			$('#genfem').prop("checked", true);
	}
	valusuario(true);
	valcontra(true);
	valconfcontra(true);
	valnombre(true);
	valapell(true);
	valgenero(true);
	valdni(true);
	valcorreoe(true);
	valfechanac(true);
	
	$('#input_usuario').blur(function(){valusuario();});
	$('#input_contra').blur(function(){valcontra();});
	$('#input_confcontra').blur(function(){valconfcontra();});
	$('#input_nombre').blur(function(){valnombre();});
	$('#input_apell').blur(function(){valapell();});
	$('#form_genero').blur(function(){valgenero();});
	$('#input_dni').blur(function(){valdni();});
	$('#input_correoe').blur(function(){valcorreoe();});
	$('#input_fechanac').blur(function(){valfechanac();});
	$('#genmal').click(function(){valgenero();});
	$('#genfem').click(function(){valgenero();});
	$('#registro_cancel').click(function(){localStorage.removeItem('nusuario');back();});
	$('#registro_final').click(function(){
		register();
	});
});

