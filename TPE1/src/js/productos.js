
var page_number=1;
var page_max;
//Variable global

function getGET(){
	var lock=document.location.href;
	var getString=lock.split('?');
	if(getString.length == 1) return {};
	var GET=getString[1].split('&');
	var get={};

	for (var i = 0 ; i<GET.length ; i++) {
		var tmp=GET[i].split('=');
		get[tmp[0]]=unescape(decodeURI(tmp[1]));
	}
	return get;
}


function next_page(){
	if(page_number==page_max){
		sendNotification("No hay mas productos!",'warning', 3000);
		return;
	}
	page_number=get_page();
	var s=window.location.href;
	if (page_number!=-1){	
		s = s.substring(0,s.length - 6 - page_number.length);
		page_number++;
	}
	else
		page_number=2;	
	window.location=s+"&page="+page_number;	
}


function prev_page(){
	if(page_number==1){
		sendNotification("Son los primeros productos!", 'warning', 3000);
		return;
	}
	page_number=get_page();
	var s=window.location.href;
	if (page_number!=-1){	
		if (page_number<10){
			s=s.substring(0,s.length-7);
		}else{
			s=s.substring(0,s.length-8);
		}
		
	}
	page_number--;
	window.location=s+"&page="+page_number;	
}

function get_page(){
	if (getGET().page==undefined){
		return -1;
	}else{
		return getGET().page;
	}
}

function busqueda_por_barra(get){
	if (get.search.length!=0){
		$.ajax({
			url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductsByName&name="+get.search+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
			dataType:"jsonp"
		}).done(function(data){
			setPageMax(data);
			setFilters(data);
			showElements(data);
		});
	}
	else{
		$.ajax({
			url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
			dataType:"jsonp"
		}).done(function(data){
			setPageMax(data);
			setFilters(data);
			showElements(data);
		});
	}
}

function busqueda_por_id(get){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductsByCategoryId&id="+get.id+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);
	});
}


function busqueda_por_ocasion(get){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);
	});
}

function busqueda_por_oferta(get){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);
	});
}

function busqueda_por_dropdown(get, genero, cat, tipo){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductsByCategoryId&id="+cat+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+
		(get.filters!= undefined ? (formatFilters(get.filters + "-" + tipo + "," + genero.substring(1,genero.length-1))) : "&filters=[%20{%20\"id\":%20"+tipo+",%20\"value\":%20"+genero+"%20}%20]"),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);	

	});
}

function busqueda_por_coleccion(get, color, tipo){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+
		(get.filters!= undefined ? (formatFilters(get.filters + "-" + tipo + "," + color)) : "&filters=[%20{%20\"id\":%20"+tipo+",%20\"value\":%20\""+color+"\"%20}%20]"),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);	

	});
}



function busqueda_por_nuevo(get){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);
	});
}

function busqueda_por_subcategoria(get){ 
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[%20{%20\"id\":%206,%20\"value\":%20\"Nuevo\"%20}%20]"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);		
	});
}

function buscar_todos(get){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts"+"&page_size=16"+(get.sort_key!=undefined? ("&sort_key="+get.sort_key+"&sort_order="+get.sort_order) : (""))+"&page="+page_number+(get.filters!= undefined ? (formatFilters(get.filters)) : ""),
		dataType:"jsonp"
	}).done(function(data){
		setPageMax(data);
		setFilters(data);
		showElements(data);

	});
}

function showElements(data){
	$('#cant_found').text('Se han encontrado '+ data.total +' resultados. Página '+page_number+' de '+(page_max==0?1:page_max));

	for(var i=0;i<data.products.length;i++){
		var prod= $('<div class="col-md-3">'+'<a href="producto.html?id='+data.products[i].id+'" class="thumbnail">'+'<img src='+data.products[i].imageUrl[0]+'>'+
			'<label class="label label_produ">'+data.products[i].name+'</label>'+'<div class="price">'+'<label class="label label_precio">$ '+
			data.products[i].price+'</label>'+'</div'+'</a>'+'</div');
		$("#products").append(prod);

	}
}

function setPageMax(data){
	page_max=Math.ceil(data.total/16);
}

function formatFilters(s){
	if(s.length == 0) return "";
	var arr = s.split('-');
	var ans = "&filters=[";
	var elements;
	for(var i=0 ; i<arr.length ; i++){
		elements = arr[i].split(',');
		ans = ans + (i!=0 ? "," : "") + "{\"id\":" + elements[0] + ",\"value\":\"" + elements[1] + "\"}";
	}
	ans = ans + "]";
	return ans;
}

$(document).ready(function(){

	page_number=get_page();
	if (page_number==-1)
		page_number=1;
	var get=getGET();
	if (get.search!=undefined){
		busqueda_por_barra(get);
	}else if (get.id!=undefined){
		busqueda_por_id(get);
	}else if (get.ocasion!=undefined){
		busqueda_por_ocasion(get);
	}else if(get.oferta!=undefined){
		busqueda_por_oferta(get);
	}else if(get.nuevo!=undefined){
		busqueda_por_nuevo(get);
	}else if(get.dropdown!=undefined){
		busqueda_por_dropdown(get,get.genero,get.cat,get.tipo);
	}else if(get.verano!=undefined){
		busqueda_por_coleccion(get,"Verde",4);
	}else if(get.invierno!=undefined){
		busqueda_por_coleccion(get,"Negro",4);
	}else{
		buscar_todos(get);
	}
});

function setFilters(data){
	var div;
	if (data.total==1 || data.filters==undefined){
		$("#busq-filtros").append('<label class="label_produ no_more">No hay m&aacute;s filtros disponibles</label>');
		return;
	}
	for( var j=0 ; j<data.filters.length ; j++){
		if(data.filters[j].values.length > 1){
			if (data.filters[j].name.substring(0,5)!=='Talle'){
				div = $('<div id="filtro-' + data.filters[j].name + '" class="filtro"><label id="filtro-' + data.filters[j].name + '" class="filtro-titulo">'+ data.filters[j].name +'</label>');
				$("#busq-filtros").append(div);
				var filtros = $('<select id="filtro-' + data.filters[j].name + '" class="filtro btn-group-vertical soflow" name="'+data.filters[j].name+'">');
				filtros.append('<option selected="selected" disable value="null"></option>');
				for( var i=0 ; i<data.filters[j].values.length ; i++){
				//filtros.append($('<button type="button" class="btn btn-default" value="'+ data.filters[j].values[i] +'">'+ data.filters[j].values[i] +'</button>'));
				filtros.append($('<option class="filtro-item" value="' + data.filters[j].values[i] + '" id="' +
					data.filters[j].values[i] + '">' + data.filters[j].values[i] + '</option>'));
			}
			filtros.append($('</select>'));
			$("#busq-filtros").append(filtros);
			div = $('</div>'); 
			$("#busq-filtros").append(div);
		}
	}
}

if ($('#busq-filtros').is(':empty')){
	$('#busq-filtros').append('<label class="label_produ no_more">No hay m&aacute;s filtros disponibles</label>');

}
}

function filtrar(){
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAllAttributes",
		dataType:"jsonp"
	}).done(function(data){
		var s=window.location.href;
		var opt = document.getElementsByTagName('option');
		var sel=document.getElementsByTagName('select');
		var get = getGET();
		var add = get.filters;
		if(get.page != undefined){
			page_number=1;
			s = s.substring(0,s.length - 6 - get.page.length);
		}
		if(add == undefined) add = "";
		s = s.substring(0, s.length - add.length);

		for (var i=0;i<sel.length;i++){
			var selected= sel[i].options[sel[i].selectedIndex];
			if(selected.value!=='null'){
				for(var j=0 ; j<data.attributes.length ; j++){
					if(data.attributes[j].name == sel[i].name){
						add = add + "-" + data.attributes[j].id + "," + selected.value;
					}
				}
			}
		}

		if(get.filters == undefined){
			add = add.substring(1,add.length);
			add = "&filters=" + add;
		}else if(get.filters.length == 0){
			add = add.substring(1,add.length);
		}
		
		window.location = s + add;
	});
}

function ordenar_por(){
	var s=window.location.href;
	// var j=s.indexOf("&sort_order");
	// if (j!==-1){
	// 	s=s.substring()
	// }
	var i=s.indexOf("&filters");
	if (i==-1)
		i=s.indexOf("&page");
	if (i==-1)
		i=s.length;
	var before=s.substring(0,i);
	var after=s.substring(i,s.length);
	s=before+getType()+after;
	window.location=s;
}


function getType(){
	var e = document.getElementById("ordenar");
	var tipo= e.options[e.selectedIndex];
	if (tipo.value=='null')
		return "";
	switch(tipo.value){
		case "PA": return "&sort_order=asc&sort_key=precio";
		case "PD": return "&sort_order=desc&sort_key=precio";
		case "NA": return "&sort_order=asc&sort_key=nombre";
		case "ND": return "&sort_order=desc&sort_key=nombre";
		case "MA": return "&sort_order=asc&sort_key=marca";
		case "MD": return "&sort_order=desc&sort_key=marca";
	}
}


