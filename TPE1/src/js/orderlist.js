$(document).ready(function(){

	cargar_pedidos();

});


function cargar_pedidos(){
	if (localStorage.username==undefined){
		sendNotification("Debe ingresar antes de listar sus pedidos", 'danger', 3000);
		return;
	}
// url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
$.ajax({
	url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	dataType:"jsonp"
}).done(function(data){
	for(var i=0; i<data.orders.length;i++){
		if (data.orders[i].status>1){
			$("#lista-pedidos").append("<div class=\"pedidos-entry\"><div class=\"row\"><div class=\"col-md-2\"><span>"+
				data.orders[i].id+"</span></div>"+"<div class=\"col-md-2\"><span>"+
				getState(data.orders[i].status)+"</span></div>"+"<div class=\"col-md-2\"><span>"+
				getAddressName(data.orders[i].address)+"</span></div>"+"<div class=\"col-md-2\"><span>"+
				data.orders[i].receivedDate+"</span></div>"+"<div class=\"col-md-2\"><span>"+
				getDeliveryDate(data.orders[i].deliveredDate)+"</span></div>"+"<div  id=\"pedido-entry-nro-" + i + "\" class=\"col-md-2\"></div>"+"</div></div>");
			getTotal(data.orders[i], i);
		}
	}
});



}

function getAddressName(data){
	if (data!=null)
		return data.name;
	return "-";
}

function getState(i){
	if (i=="1")
		return "Creado";
	if (i=="2")
		return "Confirmado";
	if (i=="3")
		return "Despachado";
	if (i=="4")
		return "Entregado";
	return "Default";
}

function getDeliveryDate(date){
	if (date!=null)
		return date;
	return "-";
}

function getTotal(info, j){
// url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + info.id,
$.ajax({
	url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken + "&id=" + info.id,
	dataType:"jsonp"
}).done(function(data){
	if(data.order.items.length == 0){
		return 0;
	}else{
		var acum = 0;
		for(var i = 0 ; i < data.order.items.length ; i++){		
			acum +=(data.order.items[i].price * data.order.items[i].quantity);
		}

		//impuestos
		acum *= 1.21;

		//envio
		acum += 50;

		acum = acum.toFixed(2);
		var idstr = "#pedido-entry-nro-" + j;
		$(idstr).text('$' + acum);
	}
});
return "";
}
