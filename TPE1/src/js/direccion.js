var inp = [false, false, false, false, false, false, false, false, false];
$('.selectpicker').selectpicker();
$('.selectpicker').selectpicker({
    style: 'btn-default',
    size: 4
});
function getGET(){
	var lock=document.location.href;
	var getString=lock.split('?')[1];
	if(!getString) return "";
	var GET=getString.split('&');
	var get={};

	for (var i = 0, l=GET.length;i<l; i++) {
		var tmp=GET[i].split('=');
		get[tmp[0]]=unescape(decodeURI(tmp[1]));
	}
	return get;
}


function valname(ignorempty){
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("name");
	ndir.name = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El nombre no puede exceder los 80 caracteres o estar vac&iacute;o';
	var valid = true;
	if(!(ignorempty && !elem.value)){
		valid = true;
		if(!elem.value || elem.value.length > 80){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[0] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[0] = false;
		}
	}
}
function valstreet(ignorempty){
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("street");
	ndir.street = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El nombre de la calle no puede exceder los 80 caracteres o estar vac&iacute;o';
	var valid = true;
	if(!(ignorempty && !elem.value)){
		valid = true;
		if(!elem.value || elem.value.length > 80){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[1] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[1] = false;
		}
	}
}
function valnumber(ignorempty){
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("number");
	ndir.number = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El numero de la direcc&oacute;n no puede exceder los 6 caracteres o estar vac&iacute;o';
	var valid = true;
	if(!(ignorempty && !elem.value)){
		valid = true;
		if(!elem.value || elem.value.length > 6){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[2] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[2] = false;
		}
	}
}
function valfloor(){
	inp[3] = true;
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("floor");
	ndir.floor = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El piso no puede exceder los 3 caracteres';
	var valid = true;
		if(elem.value.length > 6){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[3] = false;
		}
}
function valgate(){
	inp[4] = true;
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("gate");
	ndir.gate = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El departamento no puede exceder los 2 caracteres';
	var valid = true;
	if(elem.value.length > 2){
		valid = false;
	}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[4] = false;
		}
}
function valprovince(){
	inp[5] = true;
	var id = $('#province option:selected').attr('value');
	var ndir = JSON.parse(localStorage.ndir);
	ndir.province = id;
	localStorage.setItem('ndir', JSON.stringify(ndir));
		valcity();
}
function valcity(){
	inp[6] = true;
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("city");
	ndir.city = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var valid = true;
	var errmes = 'Si la provincia no es la Ciudad Autonoma de Buenos Aires se necesita especificar la ciudad';
	if(ndir.province != 'C'){
		if(!elem.value)
			valid = false;
	}
	if(elem.value.length > 80){
		valid = false;
		errmess = 'La ciudad no puede exceder los 80 caracteres';
	}
	if(valid){
		var mes = document.getElementById("messages");
		if(toHTML(mes.innerHTML) == errmes)
			mes.style['display']='none';
		elem.style["border-color"]= '#5cb85c';
		inp[6] = true;
	}else{
		sendNotification(errmes, 'danger');
		elem.style["border-color"]= '#d9534f';
		inp[6] = false;
	}
}
function valzipCode(ignorempty){
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("zipCode");
	ndir.zipCode = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'El c&oacute;digo postal no puede exceder los 10 caracteres o estar vac&iacute;o';
	var valid = true;
	if(!(ignorempty && !elem.value)){
		valid = true;
		if(!elem.value || elem.value.length > 10){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[7] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[7] = false;
		}
	}
}
function valphoneNumber(ignorempty){
	var ndir = JSON.parse(localStorage.ndir);
	var elem = document.getElementById("phoneNumber");
	ndir.phoneNumber = elem.value;
	localStorage.setItem('ndir', JSON.stringify(ndir));
	var errmes = 'Las informaciones sobre el tel&eacute;fono no puede exceder los 25 caracteres o estar vac&iacute;o';
	var valid = true;
	if(!(ignorempty && !elem.value)){
		valid = true;
		if(!elem.value || elem.value.length > 25){
			valid = false;
		}
		if(valid){
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			elem.style["border-color"]= '#5cb85c';
			inp[8] = true;
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[8] = false;
		}
	}
}

function showProvince(province){
	$.ajax({
		
 		url:'http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAllStates',
 		dataType:"jsonp"
 	}).done(function(data){
 		if(!data.error){
 			var html = '';
 			var tosel = true;
 			$.each(data.states, function(index, elem){
 				if(tosel && (!province || province == elem.stateId) ){
 					tosel = false;
 					html += '<option selected="selected" value="'+elem.stateId+'">';
 				}else{
 					html += '<option value="'+elem.stateId+'">';
 				}
 				html += elem.stateId + ' - ' + toHTML(elem.name)+ '</option>';
 			});
 			document.getElementById('province').innerHTML = html;
 			$('.selectpicker').selectpicker('refresh');
 			setTimeout(function (){valprovince();}, 100);
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error cargando las provinzias disponibles";
 					break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}
function updDireccion(elem){
	var url = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress&username='+localStorage.username
	+'&authentication_token='+localStorage.auttoken+'&address={"id":'+elem.id+',"name":"'+elem.name+'","street":"'+elem.street
	+'","number":"'+elem.number+'","province":"'+elem.province+'","zipCode":"'+elem.zipCode+'","phoneNumber":"'+elem.phoneNumber+'"';
	if(elem.floor)
		url += ',"floor":"'+elem.floor+'"';
	if(elem.gate)
		url += ',"gate":"'+elem.gate+'"';
	if(elem.city)
		url += ',"city":"'+elem.city+'"';
	url +='}';
	$.ajax({
		
 		url:url,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(!data.error){
 			var iu = JSON.parse(localStorage.infousuar);
 			iu.dir[elem.id] = elem;
 			iu.dir[elem.id].state = 'act';
 			delete localStorage['ndir'];
 			localStorage.setItem('infousuar', JSON.stringify(iu));
 			back();
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cambiando la direcci&oacute;n, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cambiando tus datos, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}

function getTarjetas(nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.creditCards){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.tar = new Object();
 	 	 			$.each(data.creditCards, function (index, credits){
 	 	 				credits.state = 'act';
 	 	 				infousuar.tar[credits.id] =credits;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 	 			
 	 			}else{
 	 				getTarjetas(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus tarjetas, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}
function getAdresses( nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.addresses){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.dir = new Object();
 	 	 			$.each(data.addresses, function (index, adress){
 	 	 				adress.state = 'act';
 	 	 				infousuar.dir[adress.id] =adress;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 			}else{
 	 				getAdresses(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus direcciones, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}

function newDireccion(elem){
	var url = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateAddress&username='+localStorage.username
	+'&authentication_token='+localStorage.auttoken+'&address={"name":"'+elem.name+'","street":"'+elem.street
	+'","number":"'+elem.number+'","province":"'+elem.province+'","zipCode":"'+elem.zipCode+'","phoneNumber":"'+elem.phoneNumber+'"';
	if(elem.floor)
		url += ',"floor":"'+elem.floor+'"';
	if(elem.gate)
		url += ',"gate":"'+elem.gate+'"';
	if(elem.city)
		url += ',"city":"'+elem.city+'"';
	url +='}';
	
	$.ajax({
		
 		url:url,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(data.address){
 			var iu = JSON.parse(localStorage.infousuar);
 			iu.dir[data.address.id] = data.address;
 			iu.dir[data.address.id].state = 'act';
 			localStorage.setItem('infousuar', JSON.stringify(iu));
 			delete localStorage['ndir'];
 			back();
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error agregando la nueva direcci&oacute;n, intenta m&aacute;s tarde";
 			}
 			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}

$(document).ready(function(){
	$('#name').blur(function(){valname();});
	$('#street').blur(function(){valstreet();});
	$('#number').blur(function(){valnumber();});
	$('#floor').blur(function(){valfloor();});
	$('#gate').blur(function(){valgate();});
	$('#province').change(function(){valprovince();});
	$('#city').blur(function(){valcity();});
	$('#zipCode').blur(function(){valzipCode();});
	$('#phoneNumber').blur(function(){valphoneNumber();});
	$('#canc').click(function(){localStorage.removeItem('ndir');back();});

	if(!localStorage.infousuar){
	$.ajax({
 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAccount&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(data.account){
	 		var usuario = data.account;
	 		var infousuar = {
	 				"usuario": usuario,
	 				"dir": null,
	 				"tar": null
	 		}
 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
	 		getAdresses();
	 		getTarjetas();
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 			
 		}
 	});}
	$('#conf').click(function(){
		valname();
		valstreet();
		valnumber();
		valfloor();
		valgate();
		valprovince();
		valcity();
		valzipCode();
		valphoneNumber();
		if(inp[0] && inp[1] && inp[2] && inp[3] && inp[4] && inp[5]&& inp[6] && inp[7] && inp[8]){
			var infousuar = JSON.parse(localStorage.infousuar);
			if(getGET().id){
				var dir = JSON.parse(localStorage.ndir);
				updDireccion(dir);
				
			}else{
				var dir = JSON.parse(localStorage.ndir);
				newDireccion(dir);
			}
		}else{
			sendNotification("Debes completar todo el formulario", 'danger', 3000);
		}
	});

	var ndir = null;
	if(typeof localStorage.ndir !== "undefined"){
		ndir = JSON.parse(localStorage.ndir);
	}
	if(getGET().id){
		if(!ndir || ndir.id != getGET().id){
			ndir = JSON.parse(localStorage.infousuar).dir[getGET().id];
		}
		localStorage.setItem('ndir', JSON.stringify(ndir));

		
		
		$("#titulo").text('Cambiar dirección de envío');
	}else if(!ndir){
		ndir = {"name":"", "street":"", "number":"", "floor":"" , "province":"C" , "city":""
			, "zipCode":"", "phoneNumber":""};
		localStorage.setItem('ndir', JSON.stringify(ndir));
	}
	$('#name').val(ndir.name);
	$('#street').val(ndir.street);
	$('#number').val(ndir.number);
	$('#floor').val(ndir.floor);
	$('#gate').val(ndir.gate);
	$('#city').val(ndir.city);
	$('#zipCode').val(ndir.zipCode);
	$('#phoneNumber').val(ndir.phoneNumber);
	showProvince('C');
	var empty = !(getGET() && getGET().id);
	valname(empty);
	valstreet(empty);
	valnumber(empty);
	valfloor(empty);
	valgate(empty);
	valzipCode(empty);
	valphoneNumber(empty);
	
});