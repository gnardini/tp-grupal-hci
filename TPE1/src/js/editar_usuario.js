var inp = [true, true, true];
/**
 * 
 */
function valnombre(ignorempty){
		var infousuar = JSON.parse(localStorage.infousuar);
	var elem = document.getElementById("input_nombre");
	var errmes = 'El nombre no puede exceder los 80 caracteres o estar vac&iacute;o';
	if(!(ignorempty && !elem.value)){
		if(elem.value.length > 0 && elem.value.length <= 80){
			elem.style["border-color"]= '#5cb85c';
			infousuar.usuario.firstName = elem.value;
 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
			inp[0] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[0] = false;
		}
	}
}

function valapell(ignorempty){
		var infousuar = JSON.parse(localStorage.infousuar);
	var elem = document.getElementById("input_apell");
	var errmes = 'El apellido no puede exceder los 80 caracteres o estar vac&iacute;o';
	if(!(ignorempty && !elem.value)){
		if(elem.value.length > 0 && elem.value.length <= 80){
			elem.style["border-color"]= '#5cb85c';
			infousuar.usuario.lastName = elem.value;
 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
			inp[1] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[1] = false;
		}
	}	
}


function valfechanac(ignorempty){
	var infousuar = JSON.parse(localStorage.infousuar);
	var elem = document.getElementById("input_fecha");
	var errmes1 = 'La fecha de nacimiento debe ser ingresada como est&aacute; explicado';
	var errmes2 = 'Lo sentimos pero los usuarios tienen que tener al menos 16 a&ntilde;os';
	if(!(ignorempty && !elem.value)){
		var from = elem.value.split("/");
		var d = new Date(from[2], from[1] - 1, from[0]);
		var comp = new Date(d.getFullYear() + 16, from[1] - 1, from[0]);
		
		if(elem.value && !isNaN(d.getTime()) && from[2].length >3 && comp <= new Date()){
			elem.style["border-color"]= '#5cb85c';
			infousuar.usuario.email = elem.value;
 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
			inp[2] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes1 || toHTML(mes.innerHTML) == errmes2)
				mes.style['display']='none';
		}else{
			if(comp > new Date())
				sendNotification(errmes2, 'danger');
			else
				sendNotification(errmes1, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[2] = false;
		}
	}
}

function guardarcambios(){
	
}



function getAdresses( nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.addresses){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.dir = new Object();
 	 	 			$.each(data.addresses, function (index, adress){
 	 	 				adress.state = 'act';
 	 	 				infousuar.dir[adress.id] =adress;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 	 			showDir(infousuar.dir);
 	 			}else{
 	 				getAdresses(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus direcciones, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}

function getTarjetas(nummer){
	var requ;
	if(nummer){
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username
	 		+"&authentication_token="+localStorage.auttoken+"&pageSize="+nummer,
	 		dataType:"jsonp"
	 	}
	}else{
		nummer = 8;
		requ = {
	 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllCreditCards&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
	 		dataType:"jsonp"
	 	}
	}
	$.ajax(requ).done(function(data){
 		if(!data.error){
 			if(data.creditCards){
 				if(data.total <= nummer){
 	 				var infousuar = JSON.parse(localStorage.infousuar);	
 	 	 			infousuar.tar = new Object();
 	 	 			$.each(data.creditCards, function (index, credits){
 	 	 				credits.state = 'act';
 	 	 				infousuar.tar[credits.id] =credits;
 	 	 			});
 	 	 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
 	 	 			showTar(infousuar.tar);
 	 			}else{
 	 				getTarjetas(data.total);
 	 			}
 			}
 			
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cargando tus direcciones, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
 			}
			sendNotification(mess, 'danger', 3000);
 		}
 	});
}

function showTar(tarjetas){
	var none = true;
	if(tarjetas){
		var dirHTML= '';
		$.each(tarjetas, function (index, tar){
			if(tar.state != 'del'){
				none = false;
				if(dirHTML){
					dirHTML += '<option id="'+tar.id+'">';
				}else{
					dirHTML += '<option selected="selected" id="'+tar.id+'">';
				}
				var marca = "";
				var secex = '';
				if(tar.number.indexOf('34') == 0 || tar.number.indexOf('37') == 0){
					marca = "AMERICAN EXPRESS";
					secex = "x";
				}else if(tar.number.indexOf('36') == 0){
					marca = "DINERS";
				}else if(tar.number.indexOf('51') == 0 || tar.number.indexOf('52') == 0 || tar.number.indexOf('53') == 0){
					marca = "MASTERCARD";
				}else if(tar.number.indexOf('4') == 0){
					marca = "VISA";
				}
				dirHTML += marca+": " + tar.number + '-xxx'+secex
						 +' exp: '+ tar.expirationDate[0]+tar.expirationDate[1]+'/'+tar.expirationDate[2]+tar.expirationDate[3]+ ' ';
				dirHTML += '</option>';
			}
		});
		document.getElementById('tar').innerHTML = dirHTML;
	}
	if(none){
		$('#edtar').attr("disabled", true);
		$('#eltar').attr("disabled", true);
	}
}

function showInfo(usuario){
	if(usuario){
		$('#input_nombre').val(toHTML(usuario.firstName));
		$('#input_apell').val(toHTML(usuario.lastName));
		$('#input_usuario').text(toHTML(usuario.username));
		if(usuario.gender == "M")
			$('#input_gender').text("Masculino");
		else
			$('#input_gender').text("Femenino");
			
		$('#input_dni').text(toHTML(usuario.identityCard));
		$('#input_fecha').val(datetoclient(usuario.birthDate));
		if(usuario.createdDate){
			var teil = usuario.createdDate.split(" ");
			$('#input_fechcrea').text(datetoclient(teil[0]));
		}
		if(usuario.createdDate){
			var teil = usuario.lastLoginDate.split(" ");
			$('#input_fechsesio').text(datetoclient(teil[0]));
		}
	}
}
function showDir(addresses){
	var none = true;
	if(addresses){
		var dirHTML= '';
		$.each(addresses, function (index, adress){
			if(adress.state != 'del'){
				none = false;
				if(dirHTML){
					dirHTML += '<option id="'+adress.id+'">';
				}else{
					dirHTML += '<option selected="selected" id="'+adress.id+'">';
				}
				dirHTML += adress.name + ': '+ adress.street + ' '+ adress.number+ ' ';
				if(adress.floor)
					dirHTML += adress.floor+ ' ';
				if(adress.gate)
					dirHTML += adress.gate+ ' ';
				dirHTML += adress.zipCode+ ' ';
				if(adress.city)
					dirHTML += adress.city+ ' ';
				dirHTML += adress.province+ ' ';
				dirHTML += adress.phoneNumber+ ' ';
				dirHTML += '</option>';
			}
		});
		document.getElementById('dir').innerHTML = dirHTML;
	}
	if(none){
		$('#eddir').attr("disabled", true);
		$('#eldir').attr("disabled", true);
	}
}

function delDireccion(elem){
	var url = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=DeleteAddress&username='+localStorage.username
	+'&authentication_token='+localStorage.auttoken+'&id='+elem.id;
	
	$.ajax({
 		url:url,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(!data.error){
 			var iu = JSON.parse(localStorage.infousuar);
 			delete iu.dir[elem.id];
 			localStorage.setItem('infousuar', JSON.stringify(iu));
			showDir(iu.dir);
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
 			}
 			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}

function delTarjeta(elem){
	$.ajax({
 		url:'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=DeleteCreditCard&username='+localStorage.username
		+'&authentication_token='+localStorage.auttoken+'&id='+elem.id,
 		dataType:"jsonp"
 	}).done(function(data){
 		if(!data.error){
 			var iu = JSON.parse(localStorage.infousuar);
 			delete iu.tar[elem.id];
 			localStorage.setItem('infousuar', JSON.stringify(iu));
			showTar(iu.tar);
 		}else{
 			var mess;
 			if(data.error.code){
 				switch(data.error.code){
 				case 999:
 					mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
 					break;
 				case 101:
 				case 102:
 				case 104:
 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
		 			setTimeout(function (){
		 				loggout();
		 			},2000);
 				break;
 				default:

					mess = data.error.message;
 				}
 			}else{
 				mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
 			}
 			sendNotification(mess, 'danger', 8000);
 			
 		}
 	});
}

 $(document).ready(function (){
		$('#input_fecha').blur(function(){valfechanac();});
		$('#input_nombre').blur(function(){valnombre();});
 		$('#input_apell').blur(function(){valapell();});
		$('#guardar').click(function(){guardarcambios();});
		$('#volver').click(function(){back();});
		if(!localStorage.infousuar){
			$.ajax({
		 		url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAccount&username="+localStorage.username+"&authentication_token="+localStorage.auttoken,
		 		dataType:"jsonp"
		 	}).done(function(data){
		 		if(data.account){
			 		var usuario = data.account;
			 		var infousuar = {
			 				"usuario": usuario,
			 				"dir": null,
			 				"tar": null
			 		}
		 			localStorage.setItem('infousuar', JSON.stringify(infousuar));
			 		getAdresses();
			 		getTarjetas();
			 		
		 			showInfo(usuario);
		 			
		 		}else{
		 			var mess;
		 			if(data.error.code){
		 				switch(data.error.code){
		 				case 999:
		 					mess = "Se produjo un error inesperado cargando tus datos, intente m&aacute;s tarde";
		 					break;
		 				case 101:
		 				case 102:
		 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
				 			setTimeout(function (){
				 				loggout();
				 			},2000);
		 				break;
		 				default:

							mess = data.error.message;
		 				}
		 			}else{
		 				mess = "Se produjo un error inesperado cargando tus datos, intenta m&aacute;s tarde";
		 			}
					sendNotification(mess, 'danger', 3000);
		 			
		 		}
		 	});
 	}else{
 			var infousuar = JSON.parse(localStorage.infousuar);
			showDir(infousuar.dir);
			showTar(infousuar.tar);
			showInfo(infousuar.usuario)
	}
	$('#eddir').click(function (){
		var id = $('#dir').find(':selected').attr('id');
		link('direccion.html?id='+id);
	});
	$('#edtar').click(function (){
		var id = $('#tar').find(':selected').attr('id');
		link('tarjeta.html?id='+id);
	});
	$('#eltar').click(function (){
			var id = $('#tar').find(':selected').attr('id');
 			var infousuar = JSON.parse(localStorage.infousuar);
 			delTarjeta(infousuar.tar[id]);
	});
	$('#eldir').click(function (){
		var id = $('#dir').find(':selected').attr('id');
			var infousuar = JSON.parse(localStorage.infousuar);
 			delDireccion(infousuar.dir[id]);
});
	$('#guardar').click(function (){
		if(inp[0] && inp[1] && inp[2]){
			$('#guardar').attr('disabled','disabled');
 			var infousuar = JSON.parse(localStorage.infousuar);
 			sendNotification("Se han guardado los cambios", 'success', 4000);
		$.ajax({
	 		url:'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAccount&username='+localStorage.username
			+'&authentication_token='+localStorage.auttoken+'&account={"firstName":"'+infousuar.usuario.firstName+'","lastName":"'
			+infousuar.usuario.lastName+'","gender":"'+infousuar.usuario.gender+'","identityCard":"'+infousuar.usuario.identityCard
			+'","email":"'+infousuar.usuario.email+'","birthDate":"'+infousuar.usuario.birthDate+'"}";',
	 		dataType:"jsonp"
	 	}).done(function(data){
	 		if(!data.error){
	 		}else{
	 			var mess;
	 			if(data.error.code){
	 				switch(data.error.code){
	 				case 999:
	 					mess = "Se produjo un error inesperado guardando tus datos, intente m&aacute;s tarde";
	 					break;
	 				case 101:
	 				case 102:
	 				case 103:
	 					mess ="Tenemos problemas con tu cuenta, prueba a logearte otra vez";
			 			setTimeout(function (){
			 				loggout();
			 			},2000);
	 				break;
	 				default:

						mess = data.error.message;
	 				}
	 			}else{
	 				mess = "Se produjo un error inesperado cambiando tus datos, intente m&aacute;s tarde";
	 			}
	 			sendNotification(mess, 'danger', 8000);
	 			
	 		}
	 	});
		
		
		
		setTimeout(function (){
			$('#guardar').removeAttr('disabled');
		}, 1000);
		}else{
			valnombre();
			valapell();
			valfechanac();
			sendNotification("Tienes que llenar todo el formulario correctamente antes de cambiar tus datos", 'danger', 3000);
		}
	});
 });