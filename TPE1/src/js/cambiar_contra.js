var inp = [false, false, false, false];

function valnombre(ignorempty){
	var ncon = JSON.parse(localStorage.ncon);
	var elem = document.getElementById("nombre");
	ncon.nombre = elem.value;
	localStorage.setItem('ncon', JSON.stringify(ncon));
	var errmes = 'Tu nombre de usuario tiene que ser de entre 6 y 15 caracteres';
	if(!(ignorempty && !elem.value)){
		if(elem.value.length > 5 && elem.value.length < 16){
			elem.style["border-color"]= '#5cb85c';
			inp[0] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[0] = false;
		}
	}
}
function valocon(ignorempty){
	var ncon = JSON.parse(localStorage.ncon);
	var elem = document.getElementById("ocon");
	ncon.ocon = elem.value;
	localStorage.setItem('ncon', JSON.stringify(ncon));
	var errmes = 'La contrase&ntilde;a vieja tiene que ser de entre 8 y 15 caracteres';
	if(!(ignorempty && !elem.value)){
		if(elem.value.length > 7 && elem.value.length < 16){
			elem.style["border-color"]= '#5cb85c';
			inp[1] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[1] = false;
		}
	}
}
function valncon(ignorempty){
	var ncon = JSON.parse(localStorage.ncon);
	var elem = document.getElementById("ncon");
	ncon.ncon = elem.value;
	localStorage.setItem('ncon', JSON.stringify(ncon));
	var errmes = 'La nueva contrase&ntilde;a tiene que ser de entre 8 y 15 caracteres';
	if(!(ignorempty && !elem.value)){
		if(elem.value.length > 7 && elem.value.length < 16){
			elem.style["border-color"]= '#5cb85c';
			inp[2] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes)
				mes.style['display']='none';
			valccon();
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[2] = false;
		}
	}
}
function valccon(ignorempty){
	var ncon = JSON.parse(localStorage.ncon);
	var elem = document.getElementById("ccon");
	var errmes = 'La contrase&ntilde;a confirmada no coincide con la nueva contrase&ntilde;a';
	if(!(ignorempty && !elem.value)){
		if(ncon.ncon == elem.value){
			elem.style["border-color"]= '#5cb85c';
			inp[3] = true;
			var mes = document.getElementById("messages");
			if(toHTML(mes.innerHTML) == errmes){
				mes.style['display']='none';
			}	
		}else{
			sendNotification(errmes, 'danger');
			elem.style["border-color"]= '#d9534f';
			inp[3] = false;
		}
	}
}


$(document).ready(function(){
	$('#nombre').blur(function(){valnombre();});
	$('#ocon').blur(function(){valocon();});
	$('#ncon').blur(function(){valncon();});
	$('#ccon').blur(function(){valccon();});
	$('#canc').click(function(){localStorage.removeItem('ncon');back();});
	var ncon = null;
	if(localStorage.ncon){
		ncon = JSON.parse(localStorage.ncon)
	}else{
		ncon = {"nombre":"", "ocon":"","ncon":"","ccon":""}
	}
	localStorage.setItem('ncon', JSON.stringify(ncon));
	$('#nombre').val( ncon.nombre);
	$('#ocon').val( ncon.ocon);
	$('#ncon').val( ncon.ncon);
	$('#ccon').val( ncon.ccon);
	
	$('#conf').click(function(){
		valnombre();
		valocon();
		valncon();
		valccon();
		if(inp[0] && inp[1] && inp[2] && inp[3]){
			var ncon = JSON.parse(localStorage.ncon);
			if(ncon.nombre != localStorage.username){
				sendNotification("El nombre de usuario no es valido, pon el tuyo", 'danger', 3000);
			}else{
				var url =  'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=ChangePassword&u'+
	 				'sername='+localStorage.username+'&password='+ncon.ocon+'&new_password='
	 				+ncon.ncon;
				$.ajax({
			 		url:url,
			 		dataType:"jsonp"
			 	}).done(function(data){
			 		if(!data.error){
						sendNotification("Contrase&ntilde;a cambiada con &eacute;xito", 'success', 3000);
			 			setTimeout(function (){back()},1500);
			 		}else{
			 			var mess = '';
			 			switch(data.error.code){
			 			case 999:
			 				mess = "Se produjo un error inesperado cambiando tus datos, intenta m&aacute;s tarde";
			 				break;
			 			default:
			 				mess = 'La contrase&ntilde;a vieja no es válida para esta cuenta';
		 					break;
			 			}
						sendNotification(mess, 'danger', 3000);
			 		}
			 	});
			}
		}else{
			sendNotification("Tienes que llenar todo correctamente antes de cambiar la contrase&ntilde;a", 'danger', 3000);
		}
	});
	valnombre(true);
	valocon(true);
	valncon(true);
	valccon(true);
});