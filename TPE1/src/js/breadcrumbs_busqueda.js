// function generateBreads() {

// }

function getURLParameter(param) {
	var lock=document.location.href;
	var getString=lock.split('?');
	if(getString.length == 1) return undefined;
	var GET=getString[1].split('&');
	
	for (var i = 0 ; i<GET.length ; i++) {
		var tmp=GET[i].split('=');
		if(tmp[0] == param) {
			return tmp[1];
		}
	}
	return undefined;
}

function getGender(gen) {
	if(gen == "Masculino") {
		return "Hombre";
	}

	if(gen == "Femenino") return "Mujer";
	if(gen == "Bebe") return "Ni&ntilde;o";
	return undefined;
}

function getGenderLink(gen) {

	if(gen == "Masculino") {
		return "hombre.html";
	}

	if(gen == "Femenino") return "mujer.html";
	if(gen == "Bebe") return "nino.html";
	return undefined;
}

function getCategory(cat) {
	if(cat == 1) {
		return "Calzado";
	}
	if(cat == 2) return "Indumentaria";
	if(cat == 3) return "Accesorios";
}

function getCategoryLink(cat) {
	return "busqueda.html?id=" + cat;
}


/*
breadcrumb del home esta siempre presente. Es por eso que no lo contemplo en sessionStorage. Si bread_size == 0 quiere decir
que estoy en home.
*/

$(document).ready(function(){

	sessionStorage.gender = 0;
	sessionStorage.category = 0;
	sessionStorage.catalogo = 0;

	var gender = getURLParameter("genero");
	if(gender != undefined) {
		gender = gender.substring(3, gender.length - 3);
	}

	var category = getURLParameter("id");
	if(category == undefined) {
		category = getURLParameter("cat");
	}


	if(gender == undefined && category == undefined) {
		$("#breadcrumb_id").append('<li><a href="#">Catalogo</a></li>');
		sessionStorage.catalogo = "definido";
		return;
	}

	if(gender != undefined) {
		$("#breadcrumb_id").append('<li><a href="' + getGenderLink(gender) + '">' + getGender(gender) + '</a></li>');
		sessionStorage.gender = gender;
	}

	if(category != undefined) {
		$("#breadcrumb_id").append('<li class = "active">' + getCategory(category) + '</li>');
		sessionStorage.category = category;
	}
	
})