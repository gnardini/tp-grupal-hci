var existsCurrentOrder=false;

function getGET(){
	var lock=document.location.href;
	var getString=lock.split('?')[1];
	var GET=getString.split('&');
	var get={};

	for (var i = 0, l=GET.length;i<l; i++) {
		var tmp=GET[i].split('=');
		get[tmp[0]]=unescape(decodeURI(tmp[1]));
	}
	return get;
}

function increment(){
	var i=$('#quantity').attr("value");
	$('#quantity').attr("value",parseInt(i)+1);
}

function decrement(){
	var i=$('#quantity').val();
	if (parseInt(i)>0)
		$('#quantity').attr("value",parseInt(i)-1);
}

// $(document).ready(function(){
// 	var get=getGET();
// 	$.ajax({
// 		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+get.id,
// 		dataType:"jsonp"
// 	}).done(function(data){
// 		$("#product_photos").append('<div>'+'<a class="thumbnail" onclick=change_photo(0,'+data+')>'+'<img src='+data.product.imageUrl[0]+'>'+'</a>'+'</div>');
// 		$("#product_photos").append('<div>'+'<a class="thumbnail" onclick=change_photo(1,'+data+')> '+'<img src='+data.product.imageUrl[1]+'>'+'</a>'+'</div>');
// 		$("#product_photos").append('<div>'+'<a class="thumbnail" onclick=change_photo(2,'+data+')>'+'<img src='+data.product.imageUrl[2]+'>'+'</a>'+'</div>');
// 		$("#main_photo").append('<a class="thumbnail">'+'<img id="foto_principal" src='+data.product.imageUrl[0]+'>'+'</a>');
// 	});
// });

function change_photo(param){
	$('#foto_principal').attr("src",param);
}

function getData(attr,name,init,end){
	for (var i = 0; i < attr.length; i++) {
		if (attr[i].name.substring(init,end)==name)
			return attr[i];
	};
	return null;
}

// function getColor(attr){
// 	for (var i = 0; i < attr.length; i++) {
// 		if (attr[i].name=="Color")
// 			return attr[i];
// 	};
// 	return null;
// }

// function getGenero(attr){
// 	for (var i = 0; i < attr.length; i++) {
// 		if (attr[i].name=="Genero")
// 			return attr[i];
// 	};
// 	return null;
// }

function setMainPhoto(data){
	$('#main_photo').append('<a class="thumbnail">'+'<img id="foto_principal" src='+data.product.imageUrl[0]+'>'+'</a>');
}

function setOtherPhotos(data){
	$("#product_photos").append('<div>'+'<a class="thumbnail" onclick="change_photo(\''+data.product.imageUrl[0]+'\');">'+'<img src='+data.product.imageUrl[0]+'>'+'</a>'+'</div>');
	if (data.product.imageUrl.length>1)
		$("#product_photos").append('<div>'+'<a class="thumbnail" onclick="change_photo(\''+data.product.imageUrl[1]+'\');">'+'<img src='+data.product.imageUrl[1]+'>'+'</a>'+'</div>');
	if (data.product.imageUrl.length>2)
		$("#product_photos").append('<div>'+'<a class="thumbnail" onclick="change_photo(\''+data.product.imageUrl[2]+'\');">'+'<img src='+data.product.imageUrl[2]+'>'+'</a>'+'</div>');


}

function setInfo(data){	
	$("#title").text(data.product.name);

	$("#price").text("$ "+data.product.price);

	var talle=getData(data.product.attributes,"Talle",0,5);

	if (talle!=null && talle!=undefined){
		for (var i = 0; i < talle.values.length; i++) {
			var t=talle.values[i];
			$("#talles").append('  <input type="radio" name="talle" value=\"' + t + '\"" class="input_prod">  ' + '  <label class="color">'+ t +'</label>  ');
		};
	}else{
		$("#talles").append('  <input type="radio" checked="checked" name="talle" value=unico class="input_prod"> <label class="color">&Uacute;nico</label>  ');
	}

	var color=getData(data.product.attributes,"Color",0,5);

	if (color!=null && color!=undefined){
		for (var i = 0; i < color.values.length; i++) {
			var t=color.values[i];
			$("#colores").append('  <input type="radio" name="color" value=\"' + t + '\'class="input_prod">  ' + '  <label class="color">'+ t +'</label>  ');
		};
	}else{
		$("#colores").append('  <input type="radio" checked="checked" name="color" value=unico class="input_prod"> <label class="color">&Uacute;nico</label>  ');
	}

	var oferta=getData(data.product.attributes,"Oferta",0,6);

	if (oferta!=null && oferta!=undefined){
		$("#genero").append("<span style=\"color:green\">OFERTA! </span>");
	}

	var nuevo=getData(data.product.attributes,"Nuevo",0,5);

	if (nuevo!=null && nuevo!=undefined){
		$("#genero").append("<span style=\"color:green\">Nuevo lanzamiento! </span>");
	}


	var genero=getData(data.product.attributes,"Genero",0,6);

	if (genero!=null && genero!=undefined){
		$("#genero").append("Diseñado para  ");
		for (var j = 0; j < genero.values.length; j++) {
			switch(genero.values[j]){
				case "Femenino":
				$("#genero").append('Mujer.');
				break;
				case "Masculino":
				$("#genero").append('Hombre.');
				break;
				case "Infantil":
				$("#genero").append('Niño.');
				break;

			}

		};
	}

	var marca=getData(data.product.attributes,"Marca",0,5);

	if (marca!=null && marca!=undefined){
		$("#description").append("Imperdible producto marca " + "<span class=\"bold\">"+marca.values[0]+"</span>");
	}

	var material=getData(data.product.attributes,"Material",0,8);

	if (material!=null && material!=undefined){
		$("#description").append(" de " + material.values[0] +".");
	}

	var ocasion=getData(data.product.attributes,"Ocasion: ",0,7);

	if (ocasion!=null && ocasion!=undefined){
		$("#description").append(" Ocasion "+ocasion.values[0] +".");
	}

	var edad=getData(data.product.attributes,"Edad",0,4);

	if (edad!=null && edad!=undefined){
		$("#description").append("<br> Ideal para: "+edad.values[0] +".");
	}

	var estilo=getData(data.product.attributes,"Estilo",0,6);

	if (estilo!=null && estilo!=undefined){
		$("#description").append(" Estilo: ");
		for (var k = 0; k<estilo.values.length;k++) {
			$("#description").append(estilo.values[k]);
		};

	}
}

$(document).ready(function(){
	var get=getGET();
	$.ajax({
		url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+get.id,
		dataType:"jsonp"

	}).done(function(data){
		setMainPhoto(data);
		setOtherPhotos(data);
		setInfo(data);
		cargarRelacionados(getData(data.product.attributes,"Genero",0,6));
		cargarOfertas();
	});







});

function cargarOfertas(){
	$.ajax({
		url:'http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[%20{%20\"id\":%205,%20\"value\":%20\"Oferta\"%20}%20]&page_size=29',
		dataType:"jsonp"
	}).done(function(data){
		var array=[];
		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#ofertas_producto").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+'>'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#ofertas_producto").append('<div class="col-md-1"></div>');
	});
}

//Busco del mismo genero
function cargarRelacionados(info){
	$.ajax({
		url:'http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[%20{%20\"id\":%201,%20\"value\":%20\"'+info.values[0]+'\"%20}%20]&page_size=50',
		dataType:"jsonp"
	}).done(function(data){
		var array=[];
		for (var i = 0; i <5; i++) {
			do{
				var random=parseInt(Math.random()*data.products.length);
			}while(contains(array,random));
			array[i]=random;
			var prod=data.products[random];
			$("#relacionados_producto").append('<a href="producto.html?id='+prod.id+'"'+ 'class="thumbnail col-md-2">'+'<img src='+prod.imageUrl[0]+'>'+'<label class="label label_produ">'+prod.name+'</label>'+ '<div class="price">'+'<label class="label label_precio">$'+prod.price+'</label>'+'</div>'+'</a>');
		};

		$("#relacionados_producto").append('<div class="col-md-1"></div>');
	});
}

function contains(array,elem){
	for(i=0;i<array.length;i++){
		if (array[i]==elem)
			return true;
	}
	return false;
}


function addToCart(){
	if(localStorage.username == undefined){
		window.location="iniciar_sesion.html";
	}else if (!$("input[name=talle]:checked").val()){
		sendNotification('Seleccione el talle por favor','danger', 3000);
	}else if (!$("input[name=color]:checked").val()){
		sendNotification('Seleccione el color por favor','danger', 3000);
	}else{
		var quantity = $('#quantity').attr("value");
		$.ajax({
			url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken,
			dataType:"jsonp",
			beforeSend:function(){
				document.getElementById("loop").className="col-md-1 ball";
			},
			complete:function(){
				document.getElementById("loop").className="col-md-1";
			}
		}).done(function(data1){
			$.ajax({
				url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username=" + localStorage.username + "&authentication_token=" + localStorage.auttoken +
				"&order_item={\"order\":{\"id\":" + localStorage.cartID + "},\"product\":{\"id\":" + getGET().id + "},\"quantity\":" + quantity + "}",
				dataType:"jsonp",
				beforeSend:function(){
					document.getElementById("loop").className="col-md-1 ball";
				},
				complete:function(){
					document.getElementById("loop").className="col-md-1";
				}
			}).done(function(data2){
				// TODO: Agregar mensaje apropiado
				sendNotification("Item agregado con éxito",'success', 3000);
			});
		});
	}
}





