// function generateBreads() {

// }

function getURLParameter(param) {
	var lock=document.location.href;
	var getString=lock.split('?');
	if(getString.length == 1) return undefined;
	var GET=getString[1].split('&');
	
	for (var i = 0 ; i<GET.length ; i++) {
		var tmp=GET[i].split('=');
		if(tmp[0] == param) {
			return tmp[1];
		}
	}
	return undefined;
}

function getGender(gen) {
	if(gen == "Masculino") {
		return "Hombre";
	}

	if(gen == "Femenino") return "Mujer";
	if(gen == "Bebe") return "Ni&ntilde;o";
	return undefined;
}

function getGenderLink(gen) {

	if(gen == "Masculino") {
		return "hombre.html";
	}

	if(gen == "Femenino") return "mujer.html";
	if(gen == "Bebe") return "nino.html";
	return undefined;
}

function getCategory(cat) {
	if(cat == 1) {
		return "Calzado";
	}
	if(cat == 2) return "Indumentaria";
	if(cat == 3) return "Accesorios";
}

function getCategoryLink(cat) {
	return "busqueda.html?id=" + cat;
}


/*
breadcrumb del home esta siempre presente. Es por eso que no lo contemplo en sessionStorage. Si bread_size == 0 quiere decir
que estoy en home.
*/

$(document).ready(function(){

	// alert(sessionStorage.catalogo);
	// alert(sessionStorage.category);
	// alert(!sessionStorage.gender);

	if(sessionStorage.catalogo != 0) {
		$("#breadcrumb_id").append('<li><a href="busqueda.html">Catalogo</a></li>');
	}

	if( sessionStorage.gender != 0) {
		$("#breadcrumb_id").append('<li><a href="' + getGenderLink(sessionStorage.gender) + '">' + getGender(sessionStorage.gender) + '</a></li>');
	}

	if(sessionStorage.category != 0) {
		$("#breadcrumb_id").append('<li><a href = "' + getCategoryLink(sessionStorage.category) + '">' + getCategory(sessionStorage.category) + '</li>');
	}
	
	$("#breadcrumb_id").append('<li class = "active">' + "Producto" + '</li>');

})